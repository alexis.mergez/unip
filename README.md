# UniP - Unique sub-Paths in pangenome graphs

Unique sub-Paths (UniP) is a tool that returns unique subpaths contained within a graph. A unique subpath is defined by its nodes being present in only one path.