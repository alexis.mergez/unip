#!/usr/bin/env python

"""
Unip - Unique sub-Paths in pangenome graphs

Detects unique paths within pangenome graphs in GFAv1 format.

@author: alexis.mergez@inrae.fr
@version: 0.1
"""

import argparse
import os
from src.GFA_Parser import parse_GFA
from src.tools import get_unique_nodes, get_unique_subpaths
import concurrent.futures

arg_parser = argparse.ArgumentParser(description='UniP: Unique Paths searcher')
arg_parser.add_argument(
    "--gfa",
    "-g",
    dest = "gfa",
    required = True,
    help = "GFA file"
    )  
arg_parser.add_argument(
    "--output",
    "-o",
    dest = "output",
    required = True,
    help = "Output name"
    )
arg_parser.add_argument(
    "--threads",
    "-t",
    dest = "threads",
    required = False,
    default = 1,
    type = int,
    help = "Number of threads (Default: 1)"
    )
arg_parser.add_argument(
    "--progress",
    "-P",
    dest = "progress",
    action="store_true",
    help = "Output progress to stdout"
    ) 
arg_parser.add_argument(
    "--min-length",
    "-l",
    dest = "min_length",
    required = False,
    default = 0,
    help = "Minimum length of the subpath (Default: 0)"
    )
args = arg_parser.parse_args()

# Reading GFA
Nodes, Paths = parse_GFA(args.gfa, args.progress)

# Search for unique nodes
if args.progress: print(f"[UniP] Searching unique nodes ...")
unique_nodes = get_unique_nodes(Nodes, Paths, args.progress)

# Reconstruct unique paths, path by path
unique_paths = {}

# Searching subpaths in each path
executor = concurrent.futures.ThreadPoolExecutor(max_workers=args.threads)

for path_id, path_nodes in Paths.items():
    unique_paths[path_id] = executor.submit(get_unique_subpaths, Nodes, path_id, path_nodes, unique_nodes, args.progress)

executor.shutdown(wait=True)

for path_id in unique_paths.keys():
    unique_paths[path_id] = unique_paths[path_id].result()

# Summarizing subpaths using PanSN
if args.progress: print(f"[GFA_Parser::{os.path.basename(args.gfa)}] Summarizing subpaths ...")
final_unique_path = {}
for path_id, subpaths in unique_paths.items():
    
    for subpath in subpaths:
        
        final_unique_path[f"{path_id}:{subpath['START']}-{subpath['END']}"] = [",".join(subpath["PATH"]), subpath["LENGTH"]]

# Writing to output
with open(args.output, "w") as file:
    file.write(f"PATH.ID\tPATH.LENGTH\tPATH.NODES\n")
    for subpath_id, subpath_info in final_unique_path.items():
        file.write(f"{subpath_id}\t{subpath_info[1]}\t{subpath_info[0]}\n")
