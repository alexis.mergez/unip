import numpy as np

def get_unique_nodes(Nodes, Paths, debug = False):
    """
    Returns the set of nodes traversed by a unique path
    """
    # # Getting the paths that traversed each node
    # for path_id, node_list in Paths.items():

    #     for node_id in node_list:
    #         Nodes[node_id]["PATHS"].append(path_id)

    # # Removing duplicate path (induced from cycle in paths)
    # for node_id in Nodes.keys():
    #     Nodes[node_id]["PATHS"] = np.unique(Nodes[node_id]["PATHS"]).tolist()

    # Retrieving unique nodes (i.e. traversed by a unique path)
    unique_nodes = set([node_id for node_id in Nodes.keys() if len(Nodes[node_id]["PATHS"]) == 1])

    return unique_nodes

def get_unique_subpaths(Nodes, path_id, path_nodes, unique_nodes, debug):
    """
    Returns the unique region/sub-paths from the path relative to the graph
    """
    
    if debug: print(f"[UniP] Searching unique subpaths in {path_id} ...")
    
    # Output list
    output = []
    in_subpath = False
    POS = 0

    # Traversing all nodes in current path
    for node_id in path_nodes:

        # If node is unique
        if node_id in unique_nodes:
            
            # If not in a unique sub-path 
            if not in_subpath :
                output.append(
                {"START": POS, "PATH": [node_id]}
                )

                in_subpath = True
            
            # If in a subpath
            else :
                output[-1]["PATH"].append(node_id)

        # Updating current position
        POS += Nodes[node_id]["LENGTH"]
        
        # Else, closing current unique subpath if it exists
        if in_subpath:
            output[-1]["END"] = POS
            output[-1]["LENGTH"] = output[-1]["END"] - output[-1]["START"]
            in_subpath = False
    
    # Ending subpath if not already
    if in_subpath :
        output[-1]["END"] = POS
        in_subpath = False

    return output
