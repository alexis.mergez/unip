"""
GFA_Parser
"""

import os
import gzip
import numpy as np


def parse_GFA(file_path, debug = False):

    # Getting the handle according to compression or not
    if file_path[-2:] != "gz" :
        handle = open(file_path, 'r')
    
    else :
        handle = gzip.open(file_path, 'r')

    if debug: print(f"[UniP::GFA_Parser] Reading {os.path.basename(file_path)} ...")

    # Main variables
    Nodes = {}
    Paths = {}
    
    for raw_line in handle:

        # Removing '\n' and splitting according to '\t'
        if file_path[-2:] != "gz":
            line = raw_line.strip().split("\t")
        else :
            line = raw_line.decode().strip().split("\t")

        # Segments/Nodes : Creating an empty entry to store path traversing it
        if line[0] == "S":
            Nodes[str(line[1])] = {"PATHS": [], "LENGTH": len(line[2])}

        # Paths : Adding the list of nodes the path is traversing
        elif line[0] == "P":
            Paths[str(line[1])] = [str(node_id[:-1]) for node_id in line[2].split(",")]

    handle.close()

    # Getting the paths that traversed each node
    for path_id, node_list in Paths.items():

        for node_id in node_list:
            Nodes[node_id]["PATHS"].append(path_id)

    # Removing duplicate path (induced from cycle in paths)
    for node_id in Nodes.keys():
        Nodes[node_id]["PATHS"] = np.unique(Nodes[node_id]["PATHS"]).tolist()

    if debug: print(f"[UniP::GFA_Parser] Done parsing {os.path.basename(file_path)}")

    return Nodes, Paths

    

